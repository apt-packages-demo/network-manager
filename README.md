# network-manager

Network management framework (daemon and userspace tools).  https://tracker.debian.org/network-manager

## Unofficial documentation
* ArchWiki: [*NetworkManager*](https://wiki.archlinux.org/index.php/NetworkManager)